#!/bin/bash
#
# Run post configuration.
#

OPTS=`getopt -o hf: --long help,fqdn: -n 'parse-options' -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi

eval set -- "$OPTS"

HELP=false
FQDN="localhost.localdomain"

while true; do
  case "$1" in
    -h | --help ) HELP=true; shift ;;
    -f | --fqdn ) FQDN="$2"; shift; shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

if $HELP; then
  echo "Usage: linuxConfig.sh [OPTIONS]"
	echo "Run post configuration."
	echo ""
	echo "  -h, --help	display help infomation."
	echo "  -f, --fqdn	set host fqdn."
	echo ""

	exit 0
fi

# Set the hostname
hostnamectl set-hostname "$FQDN"

if [ -d /var/tmp/bootstrap ]; then
  rm -rf /var/tmp/bootstrap
fi

# Bootstrap exists.
mkdir -p /var/tmp/bootstrap
git clone https://bitbucket.org/Wolskie/ansible /var/tmp/bootstrap/ansible

# Ensure we have the playbook
FILE="/var/tmp/bootstrap/ansible/$(hostname -s)-postconfig.yml"


# If a playbook for our system exists
# then run the playbook.
#
if [ -f "$FILE" ]; then
  ansible-playbook $FILE
else
  echo "linuxConfig.sh: no configuration found"
fi
